import 'package:dialin/Widgets/BookingForm/BookingForm.dart';
import 'package:dialin/Widgets/BookingSuccess.dart';
import 'package:dialin/Widgets/SubProducts/SubProducts.dart';
import 'package:dialin/Widgets/user/Mybookings.dart';
import 'package:dialin/Widgets/Merchant/MerchantBookings.dart';
import 'package:flutter/material.dart';
import 'Pages/loginRegister/loginPage.dart';
import 'Pages/loginRegister/registerPage.dart';
import 'main.dart';

class RouteGenerator {
  static Route<dynamic> generateroute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MyHomePage());
        break;
      case BookingForm.routname:
        return MaterialPageRoute(
            builder: (_) => BookingForm(
                  currentSelectedService: args,
                ));
        break;
      case LoginPage.routname:
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;
      case RegisterPage.routname:
        return MaterialPageRoute(builder: (_) => RegisterPage());
        break;
      case BookingSuccess.routname:
        return MaterialPageRoute(
            builder: (_) => BookingSuccess(
                  bookingStatus: args,
                ));
        break;
      case SubProducts.routname:
        return MaterialPageRoute(
            builder: (_) => SubProducts(
                  product: args,
                ));
        break;
      case MyBookings.routname:
        return MaterialPageRoute(
            builder: (_) => MyBookings(
                  userID: args,
                ));
        break;
      case MerchantBookings.routname:
        return MaterialPageRoute(
            builder: (_) => MerchantBookings(
                  userID: args,
                ));
        break;
      default:
        return MaterialPageRoute(builder: (_) => MyHomePage());
        break;
    }
  }
}
