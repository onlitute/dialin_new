import 'package:flutter/material.dart';
import '../Icons/my_flutter_app_icons.dart';

class Global {
  final String baseUrl = "http://dialin24.com/demo/";
  static bool isUserLoggedin = false;
  static String userName = "Guest";
  static String customerId = "";
  static String city;
  static String postalcode;
  static String address;
  static String country;
  static String state;
  static CurrentPage cpage;
  static String userId;
  static bool locationAvailable = false;

  static Map plumbing = {
    "subProductList": [
      "Quickbook",
      "Basin Sink",
      "Bath Fitting",
      "Blockage",
      "Tap mixer",
      "Toilet"
    ],
    "subProductIconList": [
      Icons.query_builder,
      MyFlutterApp.hands_wash,
      MyFlutterApp.bath,
      Icons.block,
      Icons.kitchen,
      MyFlutterApp.water,
    ],
    "productIcon": Icons.build,
    "productName": "Plumbing",
    "productID": "1"
  };

  static Map electrical = {
    "subProductList": [
      "TV Repair",
      "Washing Machine",
      "Wiring",
      "Others",
    ],
    "subProductIconList": [
      MyFlutterApp.tv,
      MyFlutterApp.box,
      MyFlutterApp.plug,
      MyFlutterApp.devices_other,
    ],
    "productIcon": Icons.lightbulb_outline,
    "productName": "Electrical",
    "productID": "2"
  };

  static Map applianceServiceAndRepair = {
    "subProductList": [
      "AC",
      "Microwave",
      "Refrigerator",
      "Washing Machine",
      "RO or Water purifier",
      "Geyser",
    ],
    "subProductIconList": [
      Icons.ac_unit,
      Icons.branding_watermark,
      MyFlutterApp.kitchen,
      MyFlutterApp.box,
      MyFlutterApp.water,
      Icons.hot_tub,
    ],
    "productIcon": Icons.add_to_queue,
    "productName": "Appliance",
    "productID": "3"
  };

  static Map cleaningAndPestControl = {
    "subProductList": [
      "Pest Control",
      "Bathroom",
      "Kitchen",
      "Carpet",
      "Sofa",
    ],
    "subProductIconList": [
      MyFlutterApp.bug,
      MyFlutterApp.bath,
      MyFlutterApp.kitchen,
      MyFlutterApp.tape,
      MyFlutterApp.weekend,
    ],
    "productIcon": MyFlutterApp.leaf,
    "productName": "Cleaning",
    "productID": "5"
  };

  static Map carpenter = {
    "subProductList": [],
    "subProductIconList": [],
    "productIcon": MyFlutterApp.chair,
    "productName": "Carpenter",
    "productID": "4"
  };

  static Map repair = {
    "subProductList": [],
    "subProductIconList": [],
    "productIcon": Icons.settings,
    "productName": "Repair",
    "productID": "6"
  };
}

enum CurrentPage { home, dashboard }
