class UserModel {
  String customerId;
  String userName;
  String password;
  String fullname;
  String email;
  String mobile;
  String address;
  String city;
  String state;
  String registrationDateTime;
  UserModel(
    this.customerId,
    this.userName,
    this.password,
    this.fullname,
    this.email,
    this.mobile,
    this.address,
    this.city,
    this.state,
    this.registrationDateTime,
  );

  Map toJson() => {
        'userid': customerId,
        'userName': userName,
        'password': password,
        'fullname': fullname,
        'email': email,
        'mobile': mobile,
        'address': address,
        'city': city,
        'state': state,
        'registrationDateTime': registrationDateTime,
      };
}
