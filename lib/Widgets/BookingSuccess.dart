import 'package:dialin/Class/Global.dart';
import 'package:dialin/MainDrawer.dart';
import 'package:dialin/Widgets/user/Mybookings.dart';
import 'package:flutter/material.dart';

class BookingSuccess extends StatelessWidget {
  static const String routname = "bookingsuccess";
  String bookingStatus;
  BookingSuccess({Key key, this.bookingStatus}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  String currentSelectedService;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: MainDrawer(),
        floatingActionButton: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, "/");
          },
          icon: Icon(Icons.home),
        ),
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          centerTitle: true,
          title: Image.asset(
            "assets/images/logo.png",
            width: 100,
            height: 50,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.black,
                ),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Global.city + ", " + Global.state + ", " + Global.country,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                alignment: Alignment.center,
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Status: $bookingStatus",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pushNamed(
                              context,
                              '/',
                            );
                          },
                          child: Text("Home"),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);

                            Navigator.pushNamed(context, MyBookings.routname,
                                arguments: Global.userId);
                          },
                          child: Text("My Bookings"),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
