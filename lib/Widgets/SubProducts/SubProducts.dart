import 'package:dialin/Class/Global.dart';
import 'package:dialin/Widgets/BookingForm/BookingForm.dart';
import 'package:flutter/material.dart';

class SubProducts extends StatelessWidget {
  static const String routname = "SubProducts";
  final product;

  SubProducts({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(product["subProductIconList"].length);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        title: Image.asset(
          "assets/images/logo.png",
          width: 100,
          height: 50,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.black,
                ),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Global.city + ", " + Global.state + ", " + Global.country,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 20, bottom: 20),
                padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  product["productName"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  "What do you want help with ?",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
              ),
              Divider(),
              //Carosal
              // Products
              Container(
                padding:
                    EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
                child: GridView.count(
                  // padding: EdgeInsets.all(10),
                  physics: NeverScrollableScrollPhysics(),
                  childAspectRatio: 1,
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  children: <Widget>[
                    for (var i = 0;
                        i < product["subProductIconList"].length;
                        i++)
                      SubProduct(product: product, i: i),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    Colors.blue,
                    Colors.lightBlueAccent,
                  ], begin: Alignment.bottomLeft, end: Alignment.topRight),
                ),
                padding: EdgeInsets.only(top: 20, right: 5, left: 5),
                margin: EdgeInsets.only(bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Text(
                        "Seivece at Your Doorstep",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      // width: mediaQuery.width * 0.4,
                      child: Image.asset(
                        "assets/images/family.png",
                        scale: 1,
                        width: 150,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SubProduct extends StatelessWidget {
  const SubProduct({
    Key key,
    @required this.product,
    @required this.i,
  }) : super(key: key);

  final product;
  final i;

  @override
  Widget build(BuildContext context) {
    Map selectedProduct = {
      0: product["productName"],
      1: product["subProductList"][i],
      2: product
    };
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          BookingForm.routname,
          arguments: selectedProduct,
        );
      },
      child: InkWell(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.blueAccent,
              width: 1,
            ),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          margin: EdgeInsets.all(2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                product["subProductIconList"][i],
                color: Colors.blue,
                size: 50,
              ),
              Divider(
                color: Colors.white,
              ),
              Text(
                product["subProductList"][i],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//Quickbook
//basin & sink
//bath fitting
//blockage
//tap & moxer
//Toilet
//Water tnk
//motor
//minor installation
//something else
