import 'package:flutter/material.dart';
import '../Class/Global.dart';
import 'package:provider/provider.dart';

import 'package:dialin/Providers/LocationProvider.dart';

class LocationOfuserUI extends StatelessWidget {
  const LocationOfuserUI({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<LocationProvider>(context, listen: false);
    
    return Container(
      padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
      decoration: BoxDecoration(
        color: Colors.black,
      ),
      width: MediaQuery.of(context).size.width,
      child: Text(
        Global.city,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
