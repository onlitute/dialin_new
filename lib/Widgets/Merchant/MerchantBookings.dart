import 'package:dialin/Class/Global.dart';
import 'package:dialin/Icons/my_flutter_app_icons.dart';
import 'package:dialin/Providers/MerchantProvider.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../MainDrawer.dart';

class MerchantBookings extends StatefulWidget {
  static const String routname = "MerchantBookings";
  final userID;

  MerchantBookings({Key key, this.userID}) : super(key: key);

  @override
  _MerchantBookingsState createState() => _MerchantBookingsState();
}

class _MerchantBookingsState extends State<MerchantBookings> {
  final _formKey = GlobalKey<FormState>();

  var mBookingList;
  var newTemp = false;

  final phoneCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print(widget.userID);
    final merchantProvider =
        Provider.of<MerchantProvider>(context, listen: false);
    // bookingProvider.userBookingList(userID);
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        title: Image.asset(
          "assets/images/logo.png",
          width: 100,
          height: 50,
        ),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Enter Your Phone Number",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),

                TextFormField(
                  controller: phoneCtrl,
                  decoration: InputDecoration(labelText: "Enter Phone*"),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly
                  ], // Only numbers can be entered
                  maxLength: 10,
                  validator: (v) {
                    if (v.isEmpty) {
                      return "Enter Mobile Number";
                    }
                    return null;
                  },
                ),

                // SelectServiceDropdownfield(),
                // currentselectedserviceG[1] == 0
                //     ? Container()
                //     : SelectSubServiceDropdownfield(),
                Container(
                  width: MediaQuery.of(context).size.height,
                  child: RaisedButton(
                    color: Colors.pinkAccent,
                    child: Text(
                      'Get List',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        //8446643823

                        var temp = await merchantProvider
                            .merchantBookingList(phoneCtrl.text);
                        setState(() {
                          mBookingList = temp;
                          print(temp);
                          if (temp['code'] == '200') {
                            newTemp = true;
                            print("Success!");
                          } else {
                            newTemp = false;
                            print("Boing!");
                          }
                        });
                      }
                    },
                  ),
                )
              ],
            ),
          ),
          (mBookingList != null && mBookingList["code"] == 200)
              ? ListOfOrders(data: mBookingList['data']['service_details'])
              : Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height -
                      (MediaQuery.of(context).size.height / 3),
                  child: Center(
                    child: Text(
                      "Enter Your Phone Number to search",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
        ],
      )),
    );
  }
}

class ListOfOrders extends StatelessWidget {
  const ListOfOrders({
    Key key,
    @required this.data,
  }) : super(key: key);

  final data;

  @override
  Widget build(BuildContext context) {
    print("data in list $data");
    return Column(
      children: <Widget>[
        for (var item in data)
          // item["status"] == "1" ? Text(item.toString()): Text("2"),
          item["status"] == "0" ? MerchantOrders(item: item) : Container(),
        for (var item in data)
          item["status"] == "1" ? MerchantOrders(item: item) : Container(),
      ],
    );
  }
}

class MerchantOrders extends StatelessWidget {
  const MerchantOrders({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    final merchantProvider =
        Provider.of<MerchantProvider>(context, listen: false);
    return Card(
      color: item["status"] == "1" ? Colors.greenAccent : Colors.orangeAccent,
      child: ListTile(
        onTap: () async {
          return showDialog<void>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Job Detail'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text("Name: ${item['firstName']}"),
                      Text("Mobile: ${item['phone']}"),
                      Text(
                          "Address: ${item['address']}, ${item['city']},${item['state']}, ${item['pincode']}"),
                      Text(
                          "Job: ${item['categoryName']}, ${item['subcategoryName']}"),
                      item['status'] == "1"
                          ? Text("Status: DONE")
                          : Text("Status: PENDING"),
                    ],
                  ),
                ),
                actions: <Widget>[
                  item['status'] == "1"
                      ? FlatButton(
                          child: Text('Close'),
                          onPressed: () async {
                            // item['status'] = "0";
                            // print("ID: ${item['bookingId']}, Status: ${item['status']}");

                            // await merchantProvider.changeStatusOfJob(
                            //     item['bookingId'], item['status']);
                            // // print(statusResult.toString());
                            Navigator.of(context).pop();
                          },
                        )
                      : FlatButton(
                          child: Text('Job Complete'),
                          onPressed: () async {
                            item['status'] = "1";
                            print(
                                "ID: ${item['bookingId']}, Status: ${item['status']}");
                            await merchantProvider.changeStatusOfJob(
                                item['bookingId'], item['status']);
                            // print(statusResult.toString());
                            Navigator.of(context).pop();
                          },
                        ),
                ],
              );
            },
          );
        },
        dense: true,
        leading: Text(
          item['bookingId'],
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        title: Text(
          item["firstName"],
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          "${item["phone"]}",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        trailing: Text(
          "${item['address']}\n ${item['city']}",
        ),
      ),
    );
  }
}
