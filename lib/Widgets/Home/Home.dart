import 'package:dialin/Class/Global.dart';
import 'package:dialin/Widgets/BookingForm/BookingForm.dart';
import 'package:dialin/Widgets/SubProducts/SubProducts.dart';
import 'package:flutter/material.dart';

import '../../SliderImages.dart';

class Home extends StatelessWidget {
  Home({
    Key key,
    @required this.mediaQuery,
  }) : super(key: key);

  final Size mediaQuery;

  @override
  Widget build(BuildContext context) {
    final Map plumbing = Global.plumbing;
    final Map electrical = Global.electrical;
    final Map applianceServiceAndRepair = Global.applianceServiceAndRepair;
    final Map carpenter = Global.carpenter;
    final Map cleaningAndPestControl = Global.cleaningAndPestControl;
    final Map repair = Global.repair;

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              width: mediaQuery.width,
              child: Text(
                Global.city + ", " + Global.state + ", " + Global.country,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Divider(),
            //Carosal
            Container(
              child: SliderImages(),
            ),

            // Products
            Container(
              color: Colors.blue[50],
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: GridView.count(
                // padding: EdgeInsets.all(10),
                physics: NeverScrollableScrollPhysics(),
                childAspectRatio: 1,
                shrinkWrap: true,
                crossAxisCount: 3,
                children: <Widget>[
                  HomeProduct(productDerails: plumbing),
                  HomeProduct(productDerails: electrical),
                  HomeProduct(productDerails: applianceServiceAndRepair),
                  HomeProduct(productDerails: carpenter),
                  HomeProduct(productDerails: cleaningAndPestControl),
                  HomeProduct(productDerails: repair),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  Colors.blue,
                  Colors.lightBlueAccent,
                ], begin: Alignment.bottomLeft, end: Alignment.topRight),
              ),
              padding: EdgeInsets.only(top: 20, right: 5, left: 5),
              margin: EdgeInsets.only(bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    width: mediaQuery.width * 0.5,
                    child: Text(
                      "Seivece at Your Doorstep",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    // width: mediaQuery.width * 0.4,
                    child: Image.asset(
                      "assets/images/family.png",
                      scale: 1,
                      width: 150,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HomeProduct extends StatelessWidget {
  const HomeProduct({
    Key key,
    @required this.productDerails,
  }) : super(key: key);

  final Map productDerails;

  @override
  Widget build(BuildContext context) {
    Map currentSelectedProduct = {
      0: productDerails["productName"],
      1: "0",
      2: productDerails
    };
    return GestureDetector(
      onTap: () {
        if (productDerails["subProductList"].length == 0) {
          Navigator.pushNamed(context, BookingForm.routname,
              arguments: currentSelectedProduct);
        } else {
          Navigator.pushNamed(context, SubProducts.routname,
              arguments: productDerails);
        }
      },
      child: InkWell(
        child: Container(
          // padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
          decoration: BoxDecoration(
            color: Colors.white,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          // color: Colors.cyan,
          margin: EdgeInsets.all(2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                productDerails["productIcon"],
                color: Colors.blue,
                size: 50,
              ),
              Divider(
                color: Colors.white,
              ),
              Text(
                productDerails["productName"],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
