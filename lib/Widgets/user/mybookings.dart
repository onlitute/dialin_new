import 'package:dialin/Class/Global.dart';
import 'package:dialin/Icons/my_flutter_app_icons.dart';
import 'package:dialin/Providers/BookingProvider.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../MainDrawer.dart';

class MyBookings extends StatelessWidget {
  static const String routname = "MyBookings";
  final userID;

  MyBookings({Key key, this.userID}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(userID);
    final bookingProvider =
        Provider.of<BookingProvider>(context, listen: false);
    // bookingProvider.userBookingList(userID);
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        title: Image.asset(
          "assets/images/logo.png",
          width: 100,
          height: 50,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              "Your Bookings",
              style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 18),
            ),
            Column(
              children: <Widget>[
                FutureBuilder(
                  future: bookingProvider.userBookingList(userID),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      // print(snapshot.data["data"]);
                      // ListOfOrders(data: snapshot.data["data"]);
                      return ListOfOrders(data: snapshot.data["data"]);
                    } else if (snapshot.connectionState ==
                        ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.red,
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Center(child: Text("Error: ${snapshot.hasError}"));
                    } else {
                      return Center(child: Text("Somthing went wrong!"));
                    }
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ListOfOrders extends StatelessWidget {
  const ListOfOrders({
    Key key,
    @required this.data,
  }) : super(key: key);

  final data;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        for (var item in data)
          item["categoryId"] == "1" ? PlumbingTile(item: item) : Container(),
        for (var item in data)
          item["categoryId"] == "2" ? ElectricalTile(item: item) : Container(),
        for (var item in data)
          item["categoryId"] == "3"
              ? ApplianceServiceAndRepairTile(item: item)
              : Container(),
        for (var item in data)
          item["categoryId"] == "5"
              ? CleaningAndPestControlTile(item: item)
              : Container(),
        for (var item in data)
          item["categoryId"] == "4" ? CarpenterTile(item: item) : Container(),
        for (var item in data)
          item["categoryId"] == "6" ? RepairTile(item: item) : Container(),
      ],
    );
  }
}

class PlumbingTile extends StatelessWidget {
  const PlumbingTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.plumbing["productIcon"]),
        title: Text(item["categoryName"]),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        trailing: Text("${item['address']}\n ${item['city']}"),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}

class ElectricalTile extends StatelessWidget {
  const ElectricalTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.electrical["productIcon"]),
        title: Text(item["categoryName"]),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        trailing: Text("${item['address']}\n ${item['city']}"),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}

class ApplianceServiceAndRepairTile extends StatelessWidget {
  const ApplianceServiceAndRepairTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.applianceServiceAndRepair["productIcon"]),
        title: Text(item["categoryName"]),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        trailing: Text("${item['address']}\n ${item['city']}"),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}

class CleaningAndPestControlTile extends StatelessWidget {
  const CleaningAndPestControlTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.cleaningAndPestControl["productIcon"]),
        title: Text(item["categoryName"]),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        trailing: Text("${item['address']}\n ${item['city']}"),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}

class CarpenterTile extends StatelessWidget {
  const CarpenterTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.carpenter["productIcon"]),
        title: Text(item["categoryName"]),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        trailing: Text("${item['address']}\n ${item['city']}"),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}

class RepairTile extends StatelessWidget {
  const RepairTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Global.repair["productIcon"]),
        title: Text(item["categoryName"]),
        trailing: Text("${item['address']}\n ${item['city']}"),
        subtitle: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            item['subcategoryName'] != '0'
                ? Text("${item["subcategoryName"]}")
                : Container(),
            item['status'] == '1'
                ? Text(
                    "Job completed",
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  )
                : Container(),
          ],
        ),
        isThreeLine: item['subcategoryName'] != '0' ? true : false,
      ),
    );
  }
}
