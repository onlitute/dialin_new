import 'package:dialin/Pages/loginRegister/loginPage.dart';
import 'package:dialin/Widgets/BookingSuccess.dart';
import 'package:flutter/material.dart';
import 'package:dialin/Class/Global.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import '../../Providers/BookingProvider.dart';

Map currentselectedserviceG;
String currentSelectedProductG, currentSelectedSubProductG;

class BookingForm extends StatelessWidget {
  BookingForm({this.currentSelectedService});
  static const String routname = "bookingform";
  final _formKey = GlobalKey<FormState>();
  Map currentSelectedService;

  final fNameCtrl = TextEditingController();
  final lNameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final phoneCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    currentselectedserviceG = currentSelectedService;

    final bookingProvider =
        Provider.of<BookingProvider>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          centerTitle: true,
          title: Image.asset(
            "assets/images/logo.png",
            width: 100,
            height: 50,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Text(
                Global.city + ", " + Global.state + ", " + Global.country,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 30, bottom: 30),
                decoration: BoxDecoration(
                  color: Colors.blue.shade50,
                ),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  currentselectedserviceG[1] == "0"
                      ? "${currentselectedserviceG[0]}"
                      : "${currentselectedserviceG[1]} in ${currentselectedserviceG[0]}",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(15),
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Enter Your Personal Information",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextFormField(
                            controller: fNameCtrl,
                            decoration:
                                InputDecoration(labelText: "First Name*"),
                            validator: (v) {
                              if (v.isEmpty) {
                                return "Enter Name";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                            controller: lNameCtrl,
                            decoration:
                                InputDecoration(labelText: "Last Name*"),
                            validator: (v) {
                              if (v.isEmpty) {
                                return "Last Name";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                            controller: emailCtrl,
                            decoration: InputDecoration(labelText: "Email*"),
                            validator: (v) {
                              if (v.isEmpty) {
                                return "Enter Email";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                            controller: phoneCtrl,
                            decoration:
                                InputDecoration(labelText: "Enter Phone*"),
                            validator: (v) {
                              if (v.isEmpty) {
                                return "Last Name";
                              }
                              return null;
                            },
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 25),
                            child: Text(
                              "Select Service",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: Text(
                              currentselectedserviceG[1] == "0"
                                  ? "${currentselectedserviceG[0]}"
                                  : "${currentselectedserviceG[1]} in ${currentselectedserviceG[0]}",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          // SelectServiceDropdownfield(),
                          // currentselectedserviceG[1] == 0
                          //     ? Container()
                          //     : SelectSubServiceDropdownfield(),
                          Container(
                            width: MediaQuery.of(context).size.height,
                            child: RaisedButton(
                              color: Colors.red,
                              child: Text(
                                'Book Now',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              onPressed: () async {
                                if (Global.isUserLoggedin) {
                                  if (_formKey.currentState.validate()) {
                                    bookingProvider.firstName = fNameCtrl.text;
                                    bookingProvider.email = emailCtrl.text;
                                    bookingProvider.lastName = lNameCtrl.text;
                                    bookingProvider.phone = phoneCtrl.text;
                                    bookingProvider.customerId = Global.userId;
                                    bookingProvider.address = Global.address;
                                    bookingProvider.city = Global.city;
                                    bookingProvider.state = Global.state;
                                    bookingProvider.pincode = Global.postalcode;
                                    if (currentselectedserviceG[1] == "0") {
                                      bookingProvider.subcatname = "0";
                                    } else {
                                      bookingProvider.subcatname =
                                          currentselectedserviceG[1];
                                    }
                                    bookingProvider.subcatname =
                                        currentselectedserviceG[1];
                                    switch (currentselectedserviceG[0]) {
                                      case "Plumbing":
                                        bookingProvider.categoryId = "1";
                                        break;
                                      case "Electrical":
                                        bookingProvider.categoryId = "2";
                                        break;
                                      case "Appliance":
                                        bookingProvider.categoryId = "3";
                                        break;
                                      case "Carpenter":
                                        bookingProvider.categoryId = "4";
                                        break;
                                      case "Cleaning":
                                        bookingProvider.categoryId = "5";
                                        break;
                                      case "Repair":
                                        bookingProvider.categoryId = "6";
                                        break;
                                    }
                                    var r = await bookingProvider.makeBooking();
                                    print(r["message"]);

                                    Navigator.pushNamed(
                                        context, BookingSuccess.routname,
                                        arguments: r["message"]);
                                  }
                                } else {
                                  Navigator.pushNamed(
                                      context, LoginPage.routname);
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class SelectServiceDropdownfield extends StatefulWidget {
  const SelectServiceDropdownfield({
    Key key,
  }) : super(key: key);

  @override
  _SelectServiceDropdownfieldState createState() =>
      _SelectServiceDropdownfieldState();
}

class _SelectServiceDropdownfieldState
    extends State<SelectServiceDropdownfield> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      onChanged: (v) {
        setState(() {
          currentSelectedProductG = v;
        });
      },
      value: currentselectedserviceG[0],
      hint: Text("Select Service*"),
      validator: (v) {
        if (v.toString().isEmpty) {
          return "Select A Service";
        }
        return null;
      },
      items: <DropdownMenuItem>[
        DropdownMenuItem(
          value: "1",
          child: Text("Plumbing"),
        ),
        DropdownMenuItem(
          value: "2",
          child: Text("Electrical"),
        ),
        DropdownMenuItem(
          value: "3",
          child: Text("Appliance"),
        ),
        DropdownMenuItem(
          value: "4",
          child: Text("Carpenter"),
        ),
        DropdownMenuItem(
          value: "5",
          child: Text("Cleaning"),
        ),
        DropdownMenuItem(
          value: "6",
          child: Text("Repair"),
        ),
      ],
    );
  }
}

class SelectSubServiceDropdownfield extends StatefulWidget {
  const SelectSubServiceDropdownfield({
    Key key,
  }) : super(key: key);

  @override
  _SelectSubServiceDropdownfieldState createState() =>
      _SelectSubServiceDropdownfieldState();
}

class _SelectSubServiceDropdownfieldState
    extends State<SelectSubServiceDropdownfield> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      onChanged: (v) {
        setState(() {
          currentSelectedSubProductG = v;
        });
      },
      value: currentselectedserviceG[1],
      hint: Text("Select Service*"),
      validator: (v) {
        if (v.toString().isEmpty) {
          return "Select A Service";
        }
        return null;
      },
      items: <DropdownMenuItem>[
        for (var i = 0;
            i < currentselectedserviceG[2]["subProductList"].length;
            i++)
          DropdownMenuItem(
            value: currentselectedserviceG[2]["subProductList"][i],
            child: Text(currentselectedserviceG[2]["subProductList"][i]),
          ),
      ],
    );
  }
}
