import 'package:dialin/Providers/BookingProvider.dart';
import 'package:dialin/Providers/LocationProvider.dart';
import 'package:dialin/Widgets/Merchant/MerchantBookings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './MainDrawer.dart';
import 'Class/Global.dart';
import 'Providers/UserProvider.dart';
import 'Providers/MerchantProvider.dart';
import 'Route_Generator.dart';
import 'Widgets/Home/Home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: LocationProvider(),
        ),
        ChangeNotifierProvider.value(
          value: UserProvider(),
        ),
        ChangeNotifierProvider.value(
          value: BookingProvider(),
        ),
        ChangeNotifierProvider.value(
          value: MerchantProvider(),
        )
      ],
      child: MaterialApp(
        title: 'Dial In 24',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: 'OpenSans',
        ),
        initialRoute: '/',
        onGenerateRoute: RouteGenerator.generateroute,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    if (!Global.isUserLoggedin) {
      final userProvider = Provider.of<UserProvider>(context, listen: false);
      userProvider.autologinuser().then((v) {
        if (Global.isUserLoggedin) {
          print("logged in");
        }
      });
    }
//Check user is from Lucknow, UP, India

    final locationProvider =
        Provider.of<LocationProvider>(context, listen: false);
    return FutureBuilder(
      future: locationProvider.getuserLocation(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print(snapshot.data);
          if (snapshot.data) {
            if (true) {
              return Scaffold(
                  appBar: AppBar(
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    iconTheme: new IconThemeData(color: Colors.black),
                    title: Image.asset(
                      "assets/images/logo.png",
                      width: 100,
                      height: 50,
                    ),
                  ),
                  drawer: MainDrawer(),
                  body: Home(
                    mediaQuery: mediaQuery,
                  ));
            } else {
              return Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  backgroundColor: Colors.white,
                  iconTheme: new IconThemeData(color: Colors.black),
                  title: Image.asset(
                    "assets/images/logo.png",
                    width: 100,
                    height: 50,
                  ),
                ),
                body: Center(
                  child: Container(
                      width: mediaQuery.width,
                      child: Text(
                        "Sorry Our Service Is Not Available In Your Area !",
                        textAlign: TextAlign.center,
                      )),
                ),
              );
            }
          } else {
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                backgroundColor: Colors.white,
                iconTheme: new IconThemeData(color: Colors.black),
                title: Image.asset(
                  "assets/images/logo.png",
                  width: 100,
                  height: 50,
                ),
              ),
              body: Center(
                child: Text("Turn on loction of your device"),
              ),
            );
          }
        }
        if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("There was a problem !"),
            ),
          );
        } else {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.red,
              ),
            ),
          );
        }
      },
    );
  }
}

//  if (Global.city != "Nagpur") {
//           return Scaffold(
//               appBar: AppBar(
//                 centerTitle: true,
//                 backgroundColor: Colors.white,
//                 iconTheme: new IconThemeData(color: Colors.black),
//                 title: Image.asset(
//                   "assets/images/logo.png",
//                   width: 100,
//                   height: 50,
//                 ),
//               ),
//               drawer: MainDrawer(),
//               body: Container(
//                 alignment: Alignment.center,
//                 child: Text(
//                     "Service is only avilable at Lucknow.\n Your are at ${Global.city} ${Global.state}, ${Global.country}"),
//               ));
//         } else {
//           return Scaffold(
//             appBar: AppBar(
//               centerTitle: true,
//               backgroundColor: Colors.white,
//               iconTheme: new IconThemeData(color: Colors.black),
//               title: Image.asset(
//                 "assets/images/logo.png",
//                 width: 100,
//                 height: 50,
//               ),
//             ),
//             drawer: MainDrawer(),
//             body: Home(mediaQuery: mediaQuery),
//           );
//         }
