import 'package:dialin/Class/Global.dart';
import 'package:dialin/Providers/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatelessWidget {
  static const String routname = "register";

  final usernameText = TextEditingController();
  final passwordText = TextEditingController();
  final cpasswordText = TextEditingController();
  final emailText = TextEditingController();
  final mobileNoText = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    final mediaQuery = MediaQuery.of(context).size;
    AppBar appBar = AppBar(
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: new IconThemeData(color: Colors.black),
      title: Image.asset(
        "assets/images/logo.png",
        width: 100,
        height: 50,
      ),
    );
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              width: mediaQuery.width,
              child: Text(
                Global.city + ", " + Global.state + ", " + Global.country,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              height: mediaQuery.height * 0.1,
              color: Colors.blue[100],
              child: Center(
                child: Text(
                  "Register",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 26, vertical: 18),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[200],
                  ),
                  borderRadius: BorderRadius.circular(1.0),
                ),
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextInputField(
                            ctrlName: usernameText,
                            lableText: "Username",
                            errorString: "Username is Required !",
                          ),
                          TextInputField(
                            ctrlName: emailText,
                            lableText: "Email",
                            errorString: "Email is Required !",
                          ),
                          TextInputField(
                            ctrlName: mobileNoText,
                            lableText: "Mobile No.",
                            errorString: "Mobile number is Required !",
                          ),
                          PasswordField(
                            passCtrl: passwordText,
                            passwordHint: "Password",
                          ),
                          PasswordField(
                            passCtrl: cpasswordText,
                            passwordHint: "Confirm Password",
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 50,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 30, bottom: 10),
                      child: RaisedButton(
                        color: Colors.blue,
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            if (passwordText.text != cpasswordText.text) {
                              Fluttertoast.showToast(
                                  msg: "Password not matching",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.grey,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            } else {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (_) {
                                    return Container(
                                      child: AlertDialog(
                                        title: Text("Creating account..."),
                                        content: Center(
                                          widthFactor: 2,
                                          heightFactor: 2,
                                          child: CircularProgressIndicator(),
                                        ),
                                      ),
                                    );
                                  });

                              userProvider.regEmail = emailText.text;
                              userProvider.regMobile = mobileNoText.text;
                              userProvider.regUsername = usernameText.text;
                              userProvider.regPassword = passwordText.text;
                              var r = await userProvider.registerUser();

                              Navigator.pop(context);

                              Fluttertoast.showToast(
                                  msg: r["message"],
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.grey,
                                  textColor: Colors.white,
                                  fontSize: 16.0);

                              if (r["code"] == 200) {
                                Navigator.pop(context);
                              }
                            }
                          }
                        },
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            FlatButton(
                child: Text(
                  "Have an account? Login Now!",
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 15,
                  ),
                ),
                onPressed: () {
                  // Navigator.pop(context);
                  Navigator.pushNamed(context, "/login");
                }),
          ],
        ),
      ),
    );
  }
}

class TextInputField extends StatelessWidget {
  const TextInputField(
      {Key key,
      @required this.ctrlName,
      @required this.lableText,
      @required this.errorString})
      : super(key: key);

  final TextEditingController ctrlName;
  final String lableText;
  final String errorString;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 10),
      elevation: 1,
      child: Padding(
        padding: EdgeInsets.only(left: 8, right: 8, bottom: 8),
        child: TextFormField(
          validator: (v) {
            if (v.isEmpty) {
              return errorString;
            }
            return null;
          },
          controller: ctrlName,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(1),
              border: InputBorder.none,
              labelText: lableText),
        ),
      ),
    );
  }
}

class PasswordField extends StatefulWidget {
  const PasswordField({
    Key key,
    @required this.passCtrl,
    @required this.passwordHint,
  }) : super(key: key);

  final TextEditingController passCtrl;
  final String passwordHint;

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 10),
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
        child: TextFormField(
          validator: (v) {
            if (v.isEmpty) {
              return "Password is required";
            }
            return null;
          },
          controller: widget.passCtrl,
          obscureText: !showPassword,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(1),
              suffix: IconButton(
                onPressed: () {
                  setState(() {
                    showPassword = !showPassword;
                  });
                },
                icon: Icon(
                  Icons.remove_red_eye,
                  size: 15,
                  color: showPassword == true ? Colors.red : Colors.black,
                ),
              ),
              border: InputBorder.none,
              hintText: widget.passwordHint),
        ),
      ),
    );
  }
}
