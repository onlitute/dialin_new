import 'package:dialin/Class/Global.dart';
import 'package:dialin/Pages/loginRegister/registerPage.dart';
import 'package:dialin/Providers/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:ui';

import 'package:provider/provider.dart';
// import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatelessWidget {
  static const String routname = "login";
  final usernameText = TextEditingController();
  final passwordText = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    AppBar appBar = AppBar(
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: new IconThemeData(color: Colors.black),
      title: Image.asset(
        "assets/images/logo.png",
        width: 100,
        height: 50,
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              width: mediaQuery.width,
              child: Text(
                Global.city + ", " + Global.state + ", " + Global.country,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              height: mediaQuery.height * 0.1,
              color: Colors.blue[100],
              child: Center(
                child: Text(
                  "Login",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 26, vertical: 18),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[200],
                  ),
                  borderRadius: BorderRadius.circular(1.0),
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextInputField(
                        ctrlName: usernameText,
                        errorString: "Username/email is Required!",
                        lableText: "Email/Username",
                      ),
                      PasswordField(passwordText: passwordText),
                      // Card(
                      //   // margin: EdgeInsets.only(top: 20),
                      //   elevation: 1,
                      //   child: Padding(
                      //     padding: EdgeInsets.only(left: 8, right: 8),
                      //     child: TextField(
                      //       controller: usernameText,
                      //       decoration: InputDecoration(
                      //           border: InputBorder.none,
                      //           labelText: "Username/Email"),
                      //     ),
                      //   ),
                      // ),
                      Container(
                        height: 50,
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 30, bottom: 10),
                        child: RaisedButton(
                          color: Colors.blue,
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              if (usernameText.text.isNotEmpty &&
                                  passwordText.text.isNotEmpty) {
                                showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (_) {
                                      return Container(
                                        child: AlertDialog(
                                          title: Text("Logging in ..."),
                                          content: Center(
                                            widthFactor: 2,
                                            heightFactor: 2,
                                            child: CircularProgressIndicator(),
                                          ),
                                        ),
                                      );
                                    });
                                userProvider.username = usernameText.text;
                                userProvider.password = passwordText.text;
                                var r = await userProvider.loginUser();

                                Navigator.pop(context);

                                Fluttertoast.showToast(
                                    msg: r["message"].toString(),
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.grey,
                                    textColor: Colors.white,
                                    fontSize: 16.0);

                                if (r["code"] == 200) {
                                  Navigator.pop(context);
                                }

                                print(r.toString());
                              } else {
                                Fluttertoast.showToast(
                                    msg:
                                        "Username and Password cannot be empty",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red[200],
                                    textColor: Colors.white,
                                    fontSize: 12.0);
                              }
                            }
                          },
                          child: Text(
                            "Login",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // FlatButton(
            //     child: Text(
            //       "Forgot Password ?",
            //       style: TextStyle(
            //         // color: Colors.white,
            //         fontSize: 15,
            //       ),
            //     ),
            //     onPressed: () {
            //       // Navigator.pop(context);
            //       Navigator.pushNamed(context, "/forgotpassword");
            //     }),
            FlatButton(
                child: Text(
                  "Dont Have an account? Register Now",
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 15,
                  ),
                ),
                onPressed: () {
                  // Navigator.pop(context);
                  Navigator.pushNamed(context, RegisterPage.routname);
                }),
          ],
        ),
      ),
    );
  }
}

class TextInputField extends StatelessWidget {
  const TextInputField(
      {Key key,
      @required this.ctrlName,
      @required this.lableText,
      @required this.errorString})
      : super(key: key);

  final TextEditingController ctrlName;
  final String lableText;
  final String errorString;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 10),
      elevation: 1,
      child: Padding(
        padding: EdgeInsets.only(left: 8, right: 8, bottom: 8),
        child: TextFormField(
          // autofocus: true,
          validator: (v) {
            if (v.isEmpty) {
              return errorString;
            }
            return null;
          },
          controller: ctrlName,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(1),
              border: InputBorder.none,
              labelText: lableText),
        ),
      ),
    );
  }
}

class PasswordField extends StatefulWidget {
  const PasswordField({
    Key key,
    @required this.passwordText,
  }) : super(key: key);

  final TextEditingController passwordText;

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 20),
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8),
        child: TextField(
          controller: widget.passwordText,
          obscureText: !showPassword,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(5),
              suffix: IconButton(
                onPressed: () {
                  setState(() {
                    showPassword = !showPassword;
                  });
                },
                icon: Icon(
                  Icons.remove_red_eye,
                  size: 15,
                  color: showPassword == true ? Colors.red : Colors.black,
                ),
              ),
              border: InputBorder.none,
              hintText: "Password"),
        ),
      ),
    );
  }
}
