import 'package:dialin/Class/Global.dart';
import 'package:dialin/Pages/loginRegister/loginPage.dart';
import 'package:dialin/Widgets/Merchant/MerchantBookings.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'Providers/UserProvider.dart';
import 'Widgets/user/Mybookings.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(
                0, MediaQuery.of(context).size.height * 0.1, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
              },
              title: Text("Welcome " + Global.userName,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          Divider(),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/');
              },
              leading: Icon(Icons.person),
              title: Text(
                "Home",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
            ),
          ),
          !Global.isUserLoggedin
              ? Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, LoginPage.routname);
                    },
                    leading: Icon(Icons.person),
                    title: Text(
                      "Login/signup",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                  ),
                )
              : Container(),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, MyBookings.routname,
                    arguments: Global.userId);
              },
              leading: Icon(Icons.book),
              title: Text("My Booking",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                //Navigator.pushNamed(context, "/login");
              },
              leading: Icon(Icons.call),
              title: Text("Customer Care",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                //Navigator.pushNamed(context, "/login");
              },
              leading: Icon(Icons.star),
              title: Text("Rate DialIn",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: ListTile(
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, MerchantBookings.routname,
                    arguments: Global.userId);
              },
              leading: Icon(Icons.book),
              title: Text("Merchant",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          Global.isUserLoggedin
              ? Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      userProvider.logoutUser();

                      Fluttertoast.showToast(
                          msg: "Logged out successfully !",
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.grey,
                          textColor: Colors.white,
                          fontSize: 16.0);
                      //Navigator.pushNamed(context, "/login");
                    },
                    leading: Icon(Icons.exit_to_app),
                    title: Text("Logout",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15)),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
