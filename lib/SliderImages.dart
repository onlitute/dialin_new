import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SliderImages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      autoPlay: true,
      viewportFraction: 1.0,
      // height: 200.0,
     aspectRatio: 16 / 9,
      items: [
        "assets/images/banner/Banner1.jpg",
        "assets/images/banner/Banner2.jpg",
      ].map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 5.0),
                decoration: BoxDecoration(color: Colors.transparent),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    decoration: BoxDecoration(color: Colors.grey),
                    child: Image.asset(
                      
                      i,
                      fit: BoxFit.cover,
                    )));
          },
        );
      }).toList(),
    );
  }
}
