import 'dart:convert';
import 'package:dialin/Class/Global.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class MerchantProvider extends Global with ChangeNotifier {
  merchantBookingList(phone) async {
    var response =
        await http.post(baseUrl + "Service/getVendorServices", body: {
      "phoneno": phone,
    });

    //  print(response.body);
    Map regResult = jsonDecode(response.body);
    if (regResult['code'] == 200) {}
    print("--------------$regResult");
    return regResult;
  }

  changeStatusOfJob(bookingid, status) async {
    await http.post(baseUrl + "Service/updateServiceStatus",
        body: {"bookingid": bookingid, "status": status});
  }
}
