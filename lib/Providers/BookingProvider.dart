import 'dart:convert';
import 'package:dialin/Class/Global.dart';
// import 'package:dialin/Model/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';

class BookingProvider extends Global with ChangeNotifier {
  String username = "";
  String password = "";

  String customerId;
  String categoryId;
  String firstName;
  String lastName;
  String email;
  String phone;
  String address;
  String city;
  String state;
  String pincode;
  String subcatname;
  String detail = "";

  makeBooking() async {
    print(
        "sending DATA = $customerId, $categoryId, $firstName, $lastName, $email, $phone, $address, $city, $state $pincode, $subcatname, $detail");
    var response = await http.post(baseUrl + "Service/booking_mobile", body: {
      "customerId": customerId,
      "categoryId": categoryId,
      "firstName": firstName,
      "lastName": lastName,
      "email": email,
      "phone": phone,
      "address": address,
      "city": city,
      "state": state,
      "pincode": pincode,
      "detail": detail,
      "subcatname": subcatname
    });

    print(response.body);
    Map regResult = jsonDecode(response.body);
    if (regResult['code'] == 200) {}
    return regResult;
  }

  userBookingList(userID) async {
    var response = await http.post(baseUrl + "Customer/booking_mobile", body: {
      "customerId": userID,
    });

    // print(response.body);
    Map regResult = jsonDecode(response.body);
    if (regResult['code'] == 200) {}
    print(regResult);
    return regResult;
  }
}
