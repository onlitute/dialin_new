import 'dart:convert';
import 'package:dialin/Class/Global.dart';
import 'package:dialin/Model/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UserProvider extends Global with ChangeNotifier {
  String username = "";
  String password = "";

  String regUsername;
  String regPassword;
  String regMobile;
  String regEmail;
  loginUser() async {
    String query = "customer/login_mobile";
    Map<String, String> fd = {"username": username, "password": password};

    var response = await http.post(baseUrl + query, body: fd);
    Map userResult = jsonDecode(response.body);
    if (userResult['code'] == 200) {
      Global.isUserLoggedin = true;
      UserModel user = UserModel(
        userResult["data"]["data"][0]["customerId"].toString(),
        userResult["data"]["data"][0]["username"].toString(),
        userResult["data"]["data"][0]["password"].toString(),
        userResult["data"]["data"][0]["fullname"].toString(),
        userResult["data"]["data"][0]["email"].toString(),
        userResult["data"]["data"][0]["mobile"].toString(),
        userResult["data"]["data"][0]["address"].toString(),
        userResult["data"]["data"][0]["city"].toString(),
        userResult["data"]["data"][0]["state"].toString(),
        userResult["data"]["data"][0]["registrationDateTime"].toString(),
      );
      Global.userName = username;
      Global.userId = userResult["data"]["data"][0]["customerId"].toString();
      SharedPreferences sfp = await SharedPreferences.getInstance();
      sfp.setString("userinfo", jsonEncode(user));
    } else {
      Global.isUserLoggedin = false;
      Global.userName = "Guest";
    }
    //print(userResult);
    return userResult;
  }

  autologinuser() async {
    SharedPreferences sfp = await SharedPreferences.getInstance();
    if (sfp.containsKey("userinfo")) {
      String userInfoString = sfp.getString("userinfo");
      Map userInfo = jsonDecode(userInfoString);
      //print(userInfo['userid'].toString());
      username = userInfo['userName'];
      password = userInfo['password'];
      await loginUser();
    }
  }

  logoutUser() async {
    SharedPreferences sfp = await SharedPreferences.getInstance();
    sfp.clear();
    Global.userName = "Guest";
    // Global.userId = "";
    Global.isUserLoggedin = false;
  }

  registerUser() async {
    var response =
        await http.post(baseUrl + "customer/registration_mobile", body: {
      "username": regUsername,
      "password": regPassword,
      "cpassword": regPassword,
      "email": regEmail,
      "mobile": regMobile,
    });

    print(response.body);
    Map regResult = jsonDecode(response.body);
    if (regResult['code'] == 200) {}
    return regResult;
  }

  resetPassword(String uid, String password, String otp) async {
    print("userid " + uid);
    print("otp " + otp);
    var response = await http.post(baseUrl + "Setnewpassword", body: {
      "Id": uid,
      "Password": password,
      "ConfirmPassword": password,
      "OTP": otp,
    });
    Map otpResult = jsonDecode(response.body);
    print(otpResult.toString());
    if (otpResult['IsSuccess']) {}
    return otpResult;
  }
}
