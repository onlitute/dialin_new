import 'package:dialin/Class/Global.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

class LocationProvider extends Global with ChangeNotifier {
  bool isLocationServiceEnabled = false;
  getuserLocation() async {
    Geolocator gl = Geolocator();
    isLocationServiceEnabled = await gl.isLocationServiceEnabled();
    if (isLocationServiceEnabled) {
      Position position =
          await gl.getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
      List<Placemark> placements = await gl.placemarkFromCoordinates(
          position.latitude, position.longitude);
      Global.city = placements[0].locality;
      Global.postalcode = placements[0].postalCode;
      Global.country = placements[0].country;
      Global.address = placements[0].subLocality;
      Global.state = placements[0].administrativeArea;
      Global.locationAvailable = true;
      print(
          "location============" + Global.city + Global.state + Global.country);
    }
    notifyListeners();
    return isLocationServiceEnabled;
  }
}
